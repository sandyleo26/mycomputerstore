package main

import (
	"log"
	"net/http"

	"github.com/sirupsen/logrus"

	"gitlab.com/sandyleo26/mycomputerstore/internal/api"
	"gitlab.com/sandyleo26/mycomputerstore/internal/db"
	"gitlab.com/sandyleo26/mycomputerstore/internal/promotion"
)

const (
	serverAddr = ":8080"
	version    = "v0.0.1"
	component  = "mycomputerstore-server"
)

func main() {
	logger := logrus.New()
	logger.
		WithField("component", component).
		WithField("version", version)
	logger.SetLevel(logrus.DebugLevel)

	store := &db.DB{}

	if err := run(logger, store); err != nil {
		log.Fatalf("run failed err:%v", err)
	}
}

func run(logger logrus.FieldLogger, store api.Store) error {

	handler := api.New(
		logger,
		store,

		promotion.NewBuyAGetBFree("43N23P", "234234"),
		promotion.NewBuyMForN("120P90", 3, 2),
		promotion.NewBuyMoreForDiscount("A304SD", 3, 0.1),
	)

	log.Printf("starting server %s ....", serverAddr)
	if err := http.ListenAndServe(serverAddr, handler); err != http.ErrServerClosed {
		return err
	}

	return nil
}
