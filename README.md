# Introduction

A small http server for my computer store

## Environment
```
OS: mac
IDE: vscode
Language: go 1.15.5
```

## Time
6h

## Run

```
# run unit tests
make test

# start the server
make

# in a different terminal
make demo
```

## Code Structure

* `cmd/server` is the `main` function.
* `internal/api` is the api handler layer
* `internal/db` is the data access layer
* `internal/promotion` is the promotion logic
* `internal/{codes,messages}` is the helper package
* `internal/middleware` is the http server middleware package

## Possible Improvements
1. Implement a simple promotion scheme which supports multiple promotion to be applied
to the same shopping cart. The promotion applied is same as when they're created.
This might not be optimum from shopper's point of view but simple to implement

2. `CheckoutRequest` should have taken a map instead of array of times to remove
duplicate items.

3. Using a in-mem store with seeded data instead of using postgres for simplicity

4. No authentication, rate limiting or cache is used in the HTTP server for simplicity
