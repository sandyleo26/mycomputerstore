package codes

const (
	CodeInvalidRequest           = "invalid_request"
	CodeSKUIsEmpty               = "sku_is_empty"
	CodeQuantityShouldBePositive = "quantity_should_be_positive"
	CodeInternalError            = "internal_error"
	CodeSKUNotFound              = "sku_not_found"
	CodeNotEnoughInventory       = "not_enough_inventory"
)
