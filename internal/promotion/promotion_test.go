package promotion

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	skuGoogleHome  = "googlehome"
	skuMBP         = "mbp"
	skuAlexa       = "alexa"
	skuRasberryPiB = "rasberryPiB"

	pricing = map[string]int64{
		skuGoogleHome:  4999,
		skuMBP:         539999,
		skuAlexa:       10950,
		skuRasberryPiB: 3000,
	}
)

func TestBuyAGetBFree(t *testing.T) {
	tcs := []struct {
		name                                        string
		promotion                                   BuyAGetBFree
		shoppingCart                                map[string]int
		wantShoppingCartAfterRemovingPromotionItems map[string]int
		wantPriceForPromotionItems                  int64
	}{
		{
			name:      "happy_path",
			promotion: BuyAGetBFree{paidSKU: skuMBP, freeSKU: skuRasberryPiB},
			shoppingCart: map[string]int{
				skuMBP:         1,
				skuRasberryPiB: 1,
			},
			wantShoppingCartAfterRemovingPromotionItems: map[string]int{skuMBP: 0, skuRasberryPiB: 0},
			wantPriceForPromotionItems:                  539999,
		},
		{
			name:      "more_free",
			promotion: BuyAGetBFree{paidSKU: skuMBP, freeSKU: skuRasberryPiB},
			shoppingCart: map[string]int{
				skuMBP:         1,
				skuRasberryPiB: 2,
			},
			wantShoppingCartAfterRemovingPromotionItems: map[string]int{skuMBP: 0, skuRasberryPiB: 1},
			wantPriceForPromotionItems:                  539999,
		},
		{
			name:      "no_free",
			promotion: BuyAGetBFree{paidSKU: skuMBP, freeSKU: skuRasberryPiB},
			shoppingCart: map[string]int{
				skuMBP:   1,
				skuAlexa: 1,
			},
			wantShoppingCartAfterRemovingPromotionItems: map[string]int{
				skuMBP:   1,
				skuAlexa: 1,
			},
			wantPriceForPromotionItems: 0,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			gotCart, gotPrice := tc.promotion.Calculate(tc.shoppingCart, pricing)
			assert.Equal(t, tc.wantShoppingCartAfterRemovingPromotionItems, gotCart)
			assert.Equal(t, tc.wantPriceForPromotionItems, gotPrice)
		})
	}
}

func TestBuyMForN(t *testing.T) {
	tcs := []struct {
		name                                        string
		promotion                                   BuyMForN
		shoppingCart                                map[string]int
		wantShoppingCartAfterRemovingPromotionItems map[string]int
		wantPriceForPromotionItems                  int64
	}{
		{
			name:      "happy_path",
			promotion: BuyMForN{sku: skuGoogleHome, m: 3, n: 2},
			shoppingCart: map[string]int{
				skuGoogleHome: 3,
			},
			wantShoppingCartAfterRemovingPromotionItems: map[string]int{skuGoogleHome: 0},
			wantPriceForPromotionItems:                  9998,
		},
		{
			name:      "m_is_less_than_n",
			promotion: BuyMForN{sku: skuGoogleHome, m: 3, n: 4},
			shoppingCart: map[string]int{
				skuGoogleHome: 3,
			},
			wantShoppingCartAfterRemovingPromotionItems: map[string]int{skuGoogleHome: 3},
			wantPriceForPromotionItems:                  0,
		},
		{
			name:      "7_equal_to_5",
			promotion: BuyMForN{sku: skuGoogleHome, m: 3, n: 2},
			shoppingCart: map[string]int{
				skuGoogleHome: 7,
			},
			wantShoppingCartAfterRemovingPromotionItems: map[string]int{skuGoogleHome: 1},
			wantPriceForPromotionItems:                  2 * 2 * 4999,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			gotCart, gotPrice := tc.promotion.Calculate(tc.shoppingCart, pricing)
			assert.Equal(t, tc.wantShoppingCartAfterRemovingPromotionItems, gotCart)
			assert.Equal(t, tc.wantPriceForPromotionItems, gotPrice)
		})
	}
}

func TestBuyMoreForDiscount(t *testing.T) {
	tcs := []struct {
		name                                        string
		promotion                                   BuyMoreForDiscount
		shoppingCart                                map[string]int
		wantShoppingCartAfterRemovingPromotionItems map[string]int
		wantPriceForPromotionItems                  int64
	}{
		{
			name:      "happy_path",
			promotion: BuyMoreForDiscount{sku: skuAlexa, threshold: 3, discount: 0.1},
			shoppingCart: map[string]int{
				skuAlexa: 3,
			},
			wantShoppingCartAfterRemovingPromotionItems: map[string]int{skuAlexa: 0},
			wantPriceForPromotionItems:                  29565,
		},
		{
			name:      "less_than_threshold",
			promotion: BuyMoreForDiscount{sku: skuAlexa, threshold: 3, discount: 0.1},
			shoppingCart: map[string]int{
				skuAlexa: 2,
			},
			wantShoppingCartAfterRemovingPromotionItems: map[string]int{skuAlexa: 2},
			wantPriceForPromotionItems:                  0,
		},
		{
			name:      "more_than_threshold",
			promotion: BuyMoreForDiscount{sku: skuAlexa, threshold: 3, discount: 0.1},
			shoppingCart: map[string]int{
				skuAlexa: 4,
			},
			wantShoppingCartAfterRemovingPromotionItems: map[string]int{skuAlexa: 0},
			wantPriceForPromotionItems:                  4 * 0.9 * 10950,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			gotCart, gotPrice := tc.promotion.Calculate(tc.shoppingCart, pricing)
			assert.Equal(t, tc.wantShoppingCartAfterRemovingPromotionItems, gotCart)
			assert.Equal(t, tc.wantPriceForPromotionItems, gotPrice)
		})
	}
}
