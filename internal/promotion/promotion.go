package promotion

import "fmt"

type BuyAGetBFree struct {
	name    string
	paidSKU string
	freeSKU string
}

func NewBuyAGetBFree(paidSKU, freeSKU string) *BuyAGetBFree {
	return &BuyAGetBFree{
		name:    fmt.Sprintf("Buy %s Get %s for Free", paidSKU, freeSKU),
		paidSKU: paidSKU,
		freeSKU: freeSKU,
	}
}

func (b BuyAGetBFree) Name() string { return b.name }
func (b BuyAGetBFree) Calculate(shoppingCart map[string]int, pricing map[string]int64) (map[string]int, int64) {
	count := min(shoppingCart[b.paidSKU], shoppingCart[b.freeSKU])
	if count != 0 {
		ret := aCopy(shoppingCart)
		ret[b.paidSKU] -= count
		ret[b.freeSKU] -= count
		return ret, int64(count) * pricing[b.paidSKU]
	}

	return shoppingCart, 0
}

type BuyMForN struct {
	name string
	sku  string
	m, n int
}

func NewBuyMForN(sku string, m, n int) *BuyMForN {
	return &BuyMForN{
		name: fmt.Sprintf("Buy %d %s at the price of %d", m, sku, n),
		sku:  sku,
		m:    m,
		n:    n,
	}
}

func (b BuyMForN) Name() string { return b.name }
func (b BuyMForN) Calculate(shoppingCart map[string]int, pricing map[string]int64) (map[string]int, int64) {

	// ignore if M is less than or equal to N
	if b.m >= b.n {
		multiple := shoppingCart[b.sku] / b.m
		if multiple != 0 {
			ret := aCopy(shoppingCart)
			ret[b.sku] -= multiple * b.m
			return ret, int64(multiple) * int64(b.n) * pricing[b.sku]
		}

	}

	return shoppingCart, 0
}

type BuyMoreForDiscount struct {
	name      string
	sku       string
	threshold int
	discount  float64
}

func NewBuyMoreForDiscount(sku string, threshold int, discount float64) *BuyMoreForDiscount {
	return &BuyMoreForDiscount{
		name:      fmt.Sprintf("Buy %d %s Get %v Discount", threshold, sku, discount),
		sku:       sku,
		threshold: threshold,
		discount:  discount,
	}
}

func (b BuyMoreForDiscount) Name() string { return b.name }
func (b BuyMoreForDiscount) Calculate(shoppingCart map[string]int, pricing map[string]int64) (map[string]int, int64) {
	if shoppingCart[b.sku] >= b.threshold {
		ret := aCopy(shoppingCart)
		ret[b.sku] = 0
		return ret, int64(float64(shoppingCart[b.sku]) * float64(pricing[b.sku]) * (1 - b.discount))
	}

	return shoppingCart, 0
}

func aCopy(m map[string]int) map[string]int {
	c := make(map[string]int)
	for k, v := range m {
		c[k] = v
	}
	return c
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
