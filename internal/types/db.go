package types

type Item struct {
	SKU      string
	Name     string
	Quantity int
	Price    int64
}
