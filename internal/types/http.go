package types

import "fmt"

type CheckoutRequest struct {
	Items []struct {
		SKU      string `json:"sku"`
		Quantity int    `json:"quantity"`
	} `josn:"items"`
}

type CheckoutResponse struct {
	Amount            int64    `json:"amount"`
	PromotionsApplied []string `json:"promotion_applied"`
}

type Error struct {
	Code    string `json:"code"`
	Message string `json:"message"`

	// used for internal logging. not exported
	causeErr error
}

func (e *Error) Error() string {
	return fmt.Sprintf("code=%s message=%s cause=%v", e.Code, e.Message, e.causeErr)
}

func NewHTTPError(code, message string, causeErr error) *Error {
	return &Error{Code: code, Message: message, causeErr: causeErr}
}
