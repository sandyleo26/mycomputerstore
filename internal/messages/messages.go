package messages

const (
	MessageInvalidRequest           = "invalid request"
	MessageSKUIsEmpty               = "sku is empty"
	MessageQuantityShouldBePositive = "quantity should be positive"
	MessageInternalError            = "internal error"
	MessageSKUNotFound              = "sku not found"
	MessageNotEnoughInventory       = "not enough inventory"
)
