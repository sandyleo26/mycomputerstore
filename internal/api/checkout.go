package api

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/sandyleo26/mycomputerstore/internal/codes"
	"gitlab.com/sandyleo26/mycomputerstore/internal/db"
	"gitlab.com/sandyleo26/mycomputerstore/internal/messages"
	"gitlab.com/sandyleo26/mycomputerstore/internal/types"
)

func (a *API) Checkout(w http.ResponseWriter, r *http.Request) {
	var req types.CheckoutRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		w.WriteHeader(http.StatusBadRequest)

		httpErr := types.NewHTTPError(codes.CodeInvalidRequest, messages.MessageInvalidRequest, err)
		a.writeError(w, r, httpErr)
		return
	}

	if httpErr := a.validateCheckoutRequest(&req); httpErr != nil {
		w.WriteHeader(http.StatusBadRequest)
		a.writeError(w, r, httpErr)
		return
	}

	// get inventory
	skus := make([]string, 0)
	for _, item := range req.Items {
		skus = append(skus, item.SKU)
	}
	inventoryItems, err := a.store.GetItems(skus)
	if err != nil {
		if errors.Is(err, db.ErrNotFound) {
			w.WriteHeader(http.StatusNotFound)
			httpErr := types.NewHTTPError(codes.CodeSKUNotFound, messages.MessageSKUNotFound, err)
			a.writeError(w, r, httpErr)
			return
		}

		httpErr := types.NewHTTPError(codes.CodeInternalError, messages.MessageInternalError, err)
		w.WriteHeader(http.StatusInternalServerError)
		a.writeError(w, r, httpErr)
		return
	}

	// check inventory vs quantity
	shoppingCart := make(map[string]int)
	for i := range req.Items {
		if req.Items[i].Quantity > inventoryItems[i].Quantity {
			httpErr := types.NewHTTPError(codes.CodeNotEnoughInventory, messages.MessageNotEnoughInventory, nil)
			w.WriteHeader(http.StatusPreconditionFailed)
			a.writeError(w, r, httpErr)
			return
		}

		// "+=" is needed in case of duplicated items
		// TODO: use map instead of array to remove duplicate
		shoppingCart[req.Items[i].SKU] += req.Items[i].Quantity
	}

	pricing := make(map[string]int64)
	for _, item := range inventoryItems {
		pricing[item.SKU] = item.Price
	}

	// calculate pricing
	var total int64
	var promotionApplied []string
	for _, promo := range a.promotions {
		var discountedItemsPrice int64
		shoppingCart, discountedItemsPrice = promo.Calculate(shoppingCart, pricing)
		if discountedItemsPrice != 0 {
			promotionApplied = append(promotionApplied, promo.Name())
		}
		total += discountedItemsPrice
	}

	total += calculateDefaultPrice(shoppingCart, pricing)
	a.writeResp(w, r, &types.CheckoutResponse{
		Amount:            total,
		PromotionsApplied: promotionApplied,
	})
}

func (a *API) validateCheckoutRequest(req *types.CheckoutRequest) error {
	for _, item := range req.Items {
		if item.SKU == "" {
			return &types.Error{
				Code:    codes.CodeSKUIsEmpty,
				Message: messages.MessageSKUIsEmpty,
			}
		}

		if item.Quantity <= 0 {
			return &types.Error{
				Code:    codes.CodeQuantityShouldBePositive,
				Message: messages.MessageQuantityShouldBePositive,
			}
		}
	}

	return nil
}

func calculateDefaultPrice(shoppingCart map[string]int, pricing map[string]int64) int64 {
	var total int64
	for k, v := range shoppingCart {
		total += pricing[k] * int64(v)
	}
	return total
}

func (a *API) writeResp(w http.ResponseWriter, r *http.Request, d interface{}) {
	a.logger.
		WithField("method", r.Method).
		WithField("path", r.URL.EscapedPath()).
		Debug("debug")

	if encodeErr := json.NewEncoder(w).Encode(d); encodeErr != nil {
		a.logger.Errorf("failed to encode d:%v into json encodeErr:%v", d, encodeErr)
	}
}

func (a *API) writeError(w http.ResponseWriter, r *http.Request, err error) {
	a.logger.
		WithField("method", r.Method).
		WithField("path", r.URL.EscapedPath()).
		WithError(err).
		Debug("debug")

	if encodeErr := json.NewEncoder(w).Encode(err); encodeErr != nil {
		a.logger.Errorf("failed to encode err:%v into json encodeErr:%v", err, encodeErr)
	}
}
