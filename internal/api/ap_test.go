package api

import (
	"os"
	"testing"

	"github.com/sirupsen/logrus"
)

var (
	testLogger = logrus.New()
)

func TestMain(m *testing.M) {
	testLogger.SetLevel(logrus.DebugLevel)
	os.Exit(m.Run())
}
