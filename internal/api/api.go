package api

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	"gitlab.com/sandyleo26/mycomputerstore/internal/middleware"
	"gitlab.com/sandyleo26/mycomputerstore/internal/types"
)

type Store interface {
	GetItems(skus []string) ([]*types.Item, error)
}

type Promotion interface {
	Calculate(shoppingCart map[string]int, pricing map[string]int64) (map[string]int, int64)
	Name() string
}

type API struct {
	logger logrus.FieldLogger
	store  Store

	promotions []Promotion
}

func New(logger logrus.FieldLogger, store Store, promotions ...Promotion) http.Handler {
	router := mux.NewRouter()
	a := &API{
		logger:     logger,
		store:      store,
		promotions: promotions,
	}

	router.HandleFunc("/api/v1/health", a.Health).Methods("GET")
	router.HandleFunc("/api/v1/checkout", a.Checkout).Methods("POST")
	router.Use(middleware.LoggingMiddleware(logger))

	return router
}

func (a *API) Health(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, "all good!")
}
