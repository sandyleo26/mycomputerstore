package api

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/sandyleo26/mycomputerstore/internal/codes"
	"gitlab.com/sandyleo26/mycomputerstore/internal/db"
	"gitlab.com/sandyleo26/mycomputerstore/internal/messages"
	"gitlab.com/sandyleo26/mycomputerstore/internal/promotion"
	"gitlab.com/sandyleo26/mycomputerstore/internal/types"
)

func TestCheckout(t *testing.T) {
	tcs := []struct {
		name         string
		promotions   []Promotion
		body         []byte
		wantStatus   int
		wantHTTPResp *types.CheckoutResponse
		wantHTTPErr  *types.Error
	}{
		{
			name: "happy_path_no_promo",
			body: []byte(`{
				"items": [
					{
						"sku": "120P90",
						"quantity": 1
					},
					{
						"sku": "43N23P",
						"quantity": 1
					},
					{
						"sku": "A304SD",
						"quantity": 1
					},
					{
						"sku": "234234",
						"quantity": 1
					}
				]
			}`),
			wantHTTPResp: &types.CheckoutResponse{
				Amount: 4999 + 539999 + 10950 + 3000,
			},
			wantStatus: http.StatusOK,
		},
		{
			name:       "happy_path_1_promo",
			promotions: []Promotion{promotion.NewBuyAGetBFree("43N23P", "234234")}, // mbp + rasberry pi
			body: []byte(`{
				"items": [
					{
						"sku": "43N23P",
						"quantity": 1
					},
					{
						"sku": "234234",
						"quantity": 1
					}
				]
			}`),
			wantHTTPResp: &types.CheckoutResponse{
				Amount:            539999,
				PromotionsApplied: []string{"Buy 43N23P Get 234234 for Free"},
			},
			wantStatus: http.StatusOK,
		},
		{
			name: "happy_path_multiple_promos",
			promotions: []Promotion{
				promotion.NewBuyAGetBFree("43N23P", "234234"),     // mbp + rasberry pi
				promotion.NewBuyMForN("120P90", 3, 2),             // 3 google home for 2
				promotion.NewBuyMoreForDiscount("A304SD", 3, 0.1), // 3 alexa for 10% discount
			},
			body: []byte(`{
				"items": [
					{
						"sku": "43N23P",
						"quantity": 1
					},
					{
						"sku": "234234",
						"quantity": 1
					},
					{
						"sku": "120P90",
						"quantity": 3
					},
					{
						"sku": "A304SD",
						"quantity": 3
					}
				]
			}`),
			wantHTTPResp: &types.CheckoutResponse{
				Amount: 539999 + 9998 + 29565,
				PromotionsApplied: []string{
					"Buy 43N23P Get 234234 for Free",
					"Buy 3 120P90 at the price of 2",
					"Buy 3 A304SD Get 0.1 Discount",
				},
			},
			wantStatus: http.StatusOK,
		},
		{
			name:       "nil_decode_fail",
			body:       nil,
			wantStatus: http.StatusBadRequest,
		},
		{
			name: "validation_fail",
			body: []byte(`{
				"items": [
					{
						"sku": "123",
						"quantity": -1
					}
				]
			}`),
			wantStatus: http.StatusBadRequest,
			wantHTTPErr: &types.Error{
				Code:    codes.CodeQuantityShouldBePositive,
				Message: messages.MessageQuantityShouldBePositive,
			},
		},
		{
			name: "not_found",
			body: []byte(`{
				"items": [
					{
						"sku": "123",
						"quantity": 1
					}
				]
			}`),
			wantStatus: http.StatusNotFound,
			wantHTTPErr: &types.Error{
				Code:    codes.CodeSKUNotFound,
				Message: messages.MessageSKUNotFound,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			handler := New(testLogger, &db.DB{}, tc.promotions...)
			testServer := httptest.NewServer(handler)
			defer testServer.Close()

			resp, err := http.Post(testServer.URL+"/api/v1/checkout", "application/json", bytes.NewReader(tc.body))
			require.NoError(t, err)
			defer resp.Body.Close()

			assert.Equal(t, tc.wantStatus, resp.StatusCode)

			if tc.wantHTTPErr != nil {
				var httpErr types.Error
				err := json.NewDecoder(resp.Body).Decode(&httpErr)
				require.NoError(t, err)
				assert.Equal(t, tc.wantHTTPErr, &httpErr)
			}

			if tc.wantHTTPResp != nil {
				var httpResp types.CheckoutResponse
				err := json.NewDecoder(resp.Body).Decode(&httpResp)
				require.NoError(t, err)
				assert.Equal(t, tc.wantHTTPResp, &httpResp)
			}
		})
	}
}
