package db

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/sandyleo26/mycomputerstore/internal/types"
)

func TestGetItem(t *testing.T) {
	tcs := []struct {
		name      string
		in        []string
		wantItems []*types.Item
		wantErr   error
	}{
		{
			name: "happy_path",
			in:   []string{"120P90", "43N23P"},
			wantItems: []*types.Item{
				inMemDB["120P90"],
				inMemDB["43N23P"],
			},
		},
		{
			name:      "empty",
			in:        []string{},
			wantItems: []*types.Item{},
		},
		{
			name:    "not_found",
			in:      []string{"foo"},
			wantErr: fmt.Errorf("sku:foo %w", ErrNotFound),
		},
	}

	db := &DB{}
	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			got, err := db.GetItems(tc.in)
			if tc.wantErr != nil {
				assert.Equal(t, tc.wantErr, err)
				return
			}

			assert.Equal(t, tc.wantItems, got)
		})
	}
}
