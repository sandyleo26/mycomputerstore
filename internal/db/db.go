package db

import (
	"errors"
	"fmt"
	"sync"

	"gitlab.com/sandyleo26/mycomputerstore/internal/types"
)

var (
	inMemDB = map[string]*types.Item{
		"120P90": {
			SKU:      "120P90",
			Name:     "Google Home",
			Price:    4999,
			Quantity: 10,
		},
		"43N23P": {
			SKU:      "43N23P",
			Name:     "MacBook Pro",
			Price:    539999,
			Quantity: 5,
		},
		"A304SD": {
			SKU:      "A304SD",
			Name:     "Alexa Speaker",
			Price:    10950,
			Quantity: 10,
		},
		"234234": {
			SKU:      "234234",
			Name:     "Raspberry Pi B",
			Price:    3000,
			Quantity: 2,
		},
	}
	inMemDBMu sync.Mutex

	ErrNotFound = errors.New("not found")
)

type DB struct{}

func (*DB) GetItems(skus []string) ([]*types.Item, error) {
	inMemDBMu.Lock()
	defer inMemDBMu.Unlock()

	res := make([]*types.Item, 0)
	for _, sku := range skus {
		if _, ok := inMemDB[sku]; !ok {
			return nil, fmt.Errorf("sku:%s %w", sku, ErrNotFound)
		}

		res = append(res, &types.Item{
			SKU:      inMemDB[sku].SKU,
			Name:     inMemDB[sku].Name,
			Price:    inMemDB[sku].Price,
			Quantity: inMemDB[sku].Quantity,
		})
	}

	return res, nil
}
