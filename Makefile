.PHONY: run
run:
	go run ./cmd/server

.PHONY: test
test:
	go test -race -v -count=1 ./...

.PHONE: demo
demo:
	# mbp + pi
	@curl  -X POST localhost:8080/api/v1/checkout \
		-d '{"items":[{"sku":"43N23P", "quantity":1}, {"sku":"234234", "quantity":1}]}' -H "Content-Type: application/json"
	# 3 google home
	@curl  -X POST localhost:8080/api/v1/checkout \
		-d '{"items":[{"sku":"120P90", "quantity":3}]}' -H "Content-Type: application/json"
	# 3 alexa
	@curl  -X POST localhost:8080/api/v1/checkout \
		-d '{"items":[{"sku":"A304SD", "quantity":3}]}' -H "Content-Type: application/json"
