module gitlab.com/sandyleo26/mycomputerstore

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.2.2
)
